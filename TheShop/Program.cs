﻿using System;

namespace TheShop {

	internal class Program {
		private static void Main(string[] args) {
            #region Shop initialization
            IShop shop = new Shop();

			shop.AddSupplier(1, "Supplier A");
			shop.AddSupplier(2, "Supplier B");
			shop.AddSupplier(3, "Supplier C");

			shop.AddArticle(1, 1, "Article 1", 450);
			shop.AddArticle(1, 2, "Article 2", 550);
			shop.AddArticle(1, 3, "Article 3", 650);
			shop.AddArticle(2, 1, "Article 1", 420);
			shop.AddArticle(2, 2, "Article 2", 520);
			shop.AddArticle(2, 3, "Article 3", 620);
			shop.AddArticle(3, 1, "Article 1", 480);
			shop.AddArticle(3, 2, "Article 2", 580);
			shop.AddArticle(3, 3, "Article 3", 680);
			#endregion

			var shopService = new ShopService(
				shop,
				DatabaseDriver.Instance,
				ConsoleLogger.Instance
			);

			try {
				// order and sell
				shopService.OrderAndSellArticle(1, 350, 10);
				shopService.OrderAndSellArticle(1, 450, 10);
				shopService.OrderAndSellArticle(1, 450, 10);
				shopService.OrderAndSellArticle(2, 600, 10);

				// print article on console
				var article = shopService.GetArticle(1);
				Console.WriteLine($"Article #{article.ID} has been recently ordered.\n");

				// print article on console
				article = shopService.GetArticle(2);
				Console.WriteLine($"Article #{article.ID} has been recently ordered.\n");
			}
			catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}

			Console.ReadKey();
		}
	}

}