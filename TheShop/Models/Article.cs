﻿namespace TheShop {

	public class Article: IEntity {
		public uint ID { get; set; } = 0;
		public string Name { get; set; }
		public double Price { get; set; } = 0.0;
		public bool IsSold { get; set; } = false;

		public Article(uint id, string name, double price) {
			ID = id;
			Name = name;
			Price = price;
        }

		public override string ToString() => $"Article #{ID} '{Name}', costs: {Price}";
	}

}
