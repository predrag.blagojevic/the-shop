﻿namespace TheShop { 

    public class Supplier : IEntity {
        public uint ID { get; set; } = 0;
        public string Name { get; set; }
        
        public Supplier(uint id, string name) {
            ID = id;
            Name = name;
        }

        public override string ToString() => $"Supplier #{ID}; Name='{Name}'";
    }

}