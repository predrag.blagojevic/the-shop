﻿using System;
using System.Collections.Generic;

namespace TheShop {

    public interface IShop {
        bool HasSupplier(uint id);
        Supplier GetSupplier(uint id);
        void AddSupplier(uint id, string name);
        bool HasArticle(uint supplierId, uint id);
        Article GetArticle(uint supplierId, uint id);
        void AddArticle(uint supplierId, uint id, string name, double price);
        Article RemoveArticle(uint supplierId, uint id);
        Article FindArticle(uint id, double maxPrice);
    }

    class Shop : IShop {
        private List<Supplier> suppliers = new List<Supplier>();
        private Dictionary<uint, List<Article>> inventory = new Dictionary<uint, List<Article>>();

        private bool hasArticle(List<Article> articles, uint id) => articles.Exists(article => article.ID == id);

        private Article getArticle(List<Article> articles, uint id) => articles.Find(article => article.ID == id);

        public bool HasSupplier(uint id) => suppliers.Exists(supplier => supplier.ID == id);

        public Supplier GetSupplier(uint id) => suppliers.Find(supplier => supplier.ID == id);

        public void AddSupplier(uint id, string name) => suppliers.Add(new Supplier(id, name));

        public bool HasArticle(uint supplierId, uint id) {
            if (!inventory.ContainsKey(supplierId)) {
                return false;
            }

            List<Article> articles = inventory[supplierId];

            return articles.Exists(article => article.ID == id);
        }

        public Article GetArticle(uint supplierId, uint id) {
            if (!inventory.ContainsKey(supplierId)) {
                return null;
            }

            List<Article> articles = inventory[supplierId];

            return getArticle(articles, id);
        }

        public void AddArticle(uint supplierId, uint id, string name, double price) {
            if (!HasSupplier(supplierId)) {
                throw new Exception($"Supplier {supplierId} doesn't exist.");
            }

            if (inventory.ContainsKey(supplierId)) {
                List<Article> articles = inventory[supplierId];
                if (!hasArticle(articles, id)) {
                    articles.Add(new Article(id, name, price));
                }
            }
            else {
                List<Article> articles = new List<Article>();
                articles.Add(new Article(id, name, price));
                inventory.Add(supplierId, articles);
            }
        }

        public Article RemoveArticle(uint supplierId, uint id) {
            if (!inventory.ContainsKey(supplierId)) {
                return null;
            }

            List<Article> articles = inventory[supplierId];
            Article article = articles.Find(item => item.ID == id);

            if (article != null) {
                articles.Remove(article);
            }

            return article;
        }

        public Article FindArticle(uint id, double maxPrice) {
            foreach (var supplier in suppliers) {
                uint supplierId = supplier.ID;

                if (!inventory.ContainsKey(supplierId)) {
                    continue;
                }

                List<Article> articles = inventory[supplierId];
                Article article = articles.Find(item => item.ID == id && item.Price < maxPrice);

                if (article != null && !article.IsSold) {
                    return article;
                }
            }

            return null;
        }
    }

}
