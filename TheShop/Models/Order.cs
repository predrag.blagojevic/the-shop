﻿using System;
using System.Text;

namespace TheShop {

    public class Order : IEntity {
        private static uint id = 0;
        public uint ID { get; set; }
        public Article Article { get; set; }
        public uint BuyerID { get; set; }
        public DateTime SoldDate { get; set; }

        public Order() {
            ID = ++id;
        }

        public Order(Article article, DateTime soldDate, uint buyerId) : this() {
            Article = article;
            SoldDate = soldDate;
            BuyerID = buyerId;
        }

        public override string ToString() => new StringBuilder()
            .AppendLine($"Order #{ID}:")
            .AppendLine($"\tArticle={Article}")
            .AppendLine($"\tSoldDate={SoldDate}")
            .AppendLine($"\tBuyerID={BuyerID}")
            .ToString();
    }

}
