﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TheShop {

	public class ShopService {
		private ILogger logger;
		private IDatabaseDriver dataSource;
		private IShop shop;

		public ShopService(IShop shop, IDatabaseDriver dataSource, ILogger logger) {
			this.logger = logger;
			this.dataSource = dataSource;
			this.shop = shop;
		}

        public void OrderAndSellArticle(uint id, double maxExpectedPrice, uint buyerId) {
			Article article = shop.FindArticle(id, maxExpectedPrice);

			if (article == null) {
				logger.Info($"Article #{id}, with price less than {maxExpectedPrice} not found.");
				return;
			}
					
			logger.Info($"{article}, cheaper than {maxExpectedPrice} is ordered.\n");
			article.IsSold = true;				
		
			Order order = new Order(article, DateTime.Now, buyerId);
			dataSource.Save(order);

			logger.Info($"{order} is sold.\n");
		}

		public Article GetArticle(uint id) {
			Order order = (Order) dataSource.GetById(id);
			return order.Article;
		}
	}

}
