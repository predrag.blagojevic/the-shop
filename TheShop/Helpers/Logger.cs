﻿using System;

namespace TheShop {
	public interface ILogger {
		void Info(string error);
		void Error(string message);
		void Debug(string message);
	}

	public sealed class ConsoleLogger: ILogger {
		private ConsoleLogger() { }

		private static readonly Lazy<ConsoleLogger> lazy = new Lazy<ConsoleLogger>(() => new ConsoleLogger());

		public static ConsoleLogger Instance {
			get { return lazy.Value; }
        }

		public void Info(string message) {
			Console.WriteLine($"INFO: {message}");
		}

		public void Error(string message) {
			Console.WriteLine($"ERROR: {message}");
		}

		public void Debug(string message) {
			Console.WriteLine($"DEBUG: {message}");
		}
	}

}
