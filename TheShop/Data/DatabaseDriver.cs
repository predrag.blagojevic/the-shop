﻿
using System;
using System.Linq;
using System.Collections.Generic;

namespace TheShop {

	public interface IDatabaseDriver {
		IEntity GetById(uint id);

		void Save(IEntity item);
	}

	public sealed class DatabaseDriver: IDatabaseDriver {
		private List<IEntity> entities = new List<IEntity>();

		private DatabaseDriver() { }

		private static readonly Lazy<DatabaseDriver> lazy = new Lazy<DatabaseDriver>(() => new DatabaseDriver());

		public static DatabaseDriver Instance {
			get { return lazy.Value; }
		}

		public IEntity GetById(uint id) {
			IEntity entity = null;

			try {
				entity = entities.Single(item => item.ID == id);
            }
			catch (Exception) {
                throw new Exception($"Entity with ID {id} not found.");
			}

			return entity;
		}

		public void Save(IEntity entity) {
			if (entity == null) {
				throw new Exception($"Entity cannot be null.");
			}

			entities.Add(entity);
		}
	}

}
