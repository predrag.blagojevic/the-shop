﻿namespace TheShop {

	public interface IEntity {
		uint ID { get; set; }
	}

}
